package com.dxc.bamboo.plan;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.api.builders.deployment.ReleaseNaming;
import com.atlassian.bamboo.specs.api.builders.permission.DeploymentPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.EnvironmentPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.builders.task.CleanWorkingDirectoryTask;
import com.atlassian.bamboo.specs.builders.task.CommandTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.model.task.ScriptTaskProperties;
import com.atlassian.bamboo.specs.util.BambooServer;

@BambooSpec
public class PolarisPlan {
	public Deployment rootObject() {
        final Deployment rootObject = new Deployment(new PlanIdentifier("ZIRUAT", "PLANUAT"), "gi-web-ws-polarisws")
            .releaseNaming(new ReleaseNaming("${bamboo.proj.build.releaseVersion}"))
            .environments(new Environment("UAT")
                    .tasks(new CleanWorkingDirectoryTask()
                            .description("CLEAN"),
                        new ScriptTask()
                            .description("download artifact")
                            .interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
                            .inlineBody("curl -u ${bamboo.NEXUS_USER}:${bamboo.NEXUS_PASSWORD} -O https://nexus.zurich.com/repository/IRLGI01-RT-RAW-hosted/gi-web-ws/polarisws/${bamboo.proj.build.releaseVersion}/zurich.polaris.ws.zip"),
//                        new CommandTask()
//                            .description("UNZIP Artifact")
//                            .executable("7zip")
//                            .argument("x ${bamboo.build.working.directory}\\zurich.polaris.ws.zip -ozurich.polaris.ws"),
                        new ScriptTask()
                            .description("UPDATE WEB.CONFIG")
                            .interpreter(ScriptTaskProperties.Interpreter.WINDOWS_POWER_SHELL)
                            .inlineBody("Remove-Item web.dev.config\nRemove-Item web.prod.config\n\n(Get-Content web.uat.config -Raw) -replace 'db_uat_username','${bamboo.db_uat_username}' | Set-Content web.uat.config\n(Get-Content web.uat.config -Raw) -replace 'db_uat_value','${bamboo.db_uat_value}' | Set-Content web.uat.config\n\nRename-Item web.uat.config web.config\n\nGet-Content web.config")
                            .workingSubdirectory("zurich.polaris.ws"),
                        new ScriptTask()
                            .description("dir")
                            .interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
                            .inlineBody("dir ${bamboo.build.working.directory}\\zurich.polaris.ws\\"),
                        new ScriptTask()
                            .description("MSDeploy with PowerShell")
                            .interpreter(ScriptTaskProperties.Interpreter.WINDOWS_POWER_SHELL)
                            .inlineBody("$Msdeploy = $Env:bamboo_capability_system_builder_command_MSDeploy\n\n# Build the expression, cater for spaces in exe path\n$buildDroppedLocation = $Env:bamboo_working_directory + '\\zurich.polaris.ws'\n$IISUrl = '\"https://CEAWW14182.zurich.uat:8172/msdeploy.axd?site=W0001-DEV-ECOMM\"'\n$IISApp = '\"W0001-DEV-ECOMM/polaris\"'\n$userName = 'CEAWW14182\\svc-Ireland-JIRAbamb'\n$password = 'p3C=T@F@6_1[WTzYzg]'\n\n$Expression = \"& '$($MsDeploy)' -verb:sync -source:iisapp='$($buildDroppedLocation)' -allowUntrusted:true -dest:iisApp='$($IISApp),ComputerName=$($IISUrl),UserName=$($userName),Password=$($password),AuthType='Basic',skipAppCreation=true' -skip:File='appsettings.json' -enableRule:AppOffline\"\n\necho .\necho .\necho $Expression\necho .\necho .\n\n# --- invoke the expression and check return code\nInvoke-Expression $Expression\nif ($LASTEXITCODE -ne 0)\n{\n     Write-Host \"An error occurred during invocation of msdeploy\" -ForeGroundColor \"Red\"\n     exit 1\n}\nelse\n{\n     Write-Host \"Success\" -ForeGroundColor \"Green\"\n}"))
                    .variables(new Variable("db_uat_username",
                            "AppLogUpdater"),
                        new Variable("db_uat_value",
                            "password")));
        return rootObject;
    }
    
    public DeploymentPermissions deploymentPermission() {
        final DeploymentPermissions deploymentPermission = new DeploymentPermissions("gi-web-ws-polarisws")
            .permissions(new Permissions()
                    .userPermissions("hurleys", PermissionType.EDIT, PermissionType.VIEW)
                    .userPermissions("correiv", PermissionType.EDIT, PermissionType.VIEW)
                    .groupPermissions("BAMBOO - IRLI02 - RT - DP - User", PermissionType.EDIT, PermissionType.VIEW)
                    .groupPermissions("BAMBOO - IRLI02 - RT - DP - Viewer", PermissionType.VIEW));
        return deploymentPermission;
    }
    
    
    public EnvironmentPermissions environmentPermission2() {
        final EnvironmentPermissions environmentPermission2 = new EnvironmentPermissions("gi-web-ws-polarisws")
            .environmentName("UAT")
            .permissions(new Permissions()
                    .userPermissions("GURU.CHAKKARAVARTHI", PermissionType.BUILD)
                    .groupPermissions("BAMBOO - IRLI02 - RT - DP - User", PermissionType.EDIT, PermissionType.VIEW)
                    .groupPermissions("BAMBOO - IRLI02 - RT - DP - Viewer", PermissionType.VIEW));
        return environmentPermission2;
    }

    
    public static void main(String... argv) {
        //By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer("https://bamboo.zurich.com");
        final PolarisPlan planSpec = new PolarisPlan();
        
        final Deployment rootObject = planSpec.rootObject();
        bambooServer.publish(rootObject);
        
//        final DeploymentPermissions deploymentPermission = planSpec.deploymentPermission();
//        bambooServer.publish(deploymentPermission);
//
//        
//        final EnvironmentPermissions environmentPermission2 = planSpec.environmentPermission2();
//        bambooServer.publish(environmentPermission2);
    }


}
