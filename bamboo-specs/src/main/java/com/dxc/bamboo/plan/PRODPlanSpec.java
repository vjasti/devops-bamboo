package com.dxc.bamboo.plan;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.builders.task.MavenTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;
import com.atlassian.bamboo.specs.util.BambooServer;

@BambooSpec
public class PRODPlanSpec {
	/**
     * Run 'main' to publish your plan.
     */
    public static void main(String[] args) throws Exception {
        // by default credentials are read from the '.credentials' file
        BambooServer bambooServer = new BambooServer("http://localhost:8085");

        Plan plan = new PRODPlanSpec().createPlan();
        bambooServer.publish(plan);

        PlanPermissions planPermission = new PRODPlanSpec().createPlanPermission(plan.getIdentifier());
        bambooServer.publish(planPermission);
    }

    PlanPermissions createPlanPermission(PlanIdentifier planIdentifier) {
        Permissions permissions = new Permissions()
                .userPermissions("vjasti", PermissionType.ADMIN)
                .loggedInUserPermissions(PermissionType.BUILD);
                

        return new PlanPermissions(planIdentifier)
                .permissions(permissions);
    }

    Project project() {
        return new Project()
                .name("Zurich-Ireland-PROD")
                .key("ZIRPROD");
    }

    Plan createPlan() {
        return new Plan(project(), "Java-prod-Plan", "PLANPROD")
                .description("Prod Plan created from Bamboo Java Specs")
                .stages(new Stage("First Stage")
                		.description("Default Stage")
                		.jobs(new Job("First Job","FSTJOB")
                				.description("First Job")
                				.tasks(new VcsCheckoutTask()
                				        .addCheckoutOfDefaultRepository()
                				        .description("Checkout Filedemo Repository"),
                				        new MavenTask()
                				        .description("Build")
                                        .goal("clean install")
                                        .jdk("JDK 1.8")
                                        .executableLabel("maven")
                                        )
                				)
                		)
                .linkedRepositories("Filedemo")
                .triggers(new RepositoryPollingTrigger())
                .planBranchManagement(new PlanBranchManagement()
                        .delete(new BranchCleanup())
                        .notificationForCommitters());
    }
}
