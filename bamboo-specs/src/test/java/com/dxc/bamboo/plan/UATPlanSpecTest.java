package com.dxc.bamboo.plan;

import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Test;

public class UATPlanSpecTest {
    @Test
    public void checkYourPlanOffline() {
        Plan plan = new UATPlanSpec().createPlan();

        EntityPropertiesBuilders.build(plan);
    }
}
